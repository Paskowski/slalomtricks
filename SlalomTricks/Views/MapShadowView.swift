//
//  swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 11.05.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

class MapShadowView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareView()
    }
    
    fileprivate func prepareView() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 3
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        let shadowPath = UIBezierPath(rect: CGRect(x: bounds.minX, y: bounds.maxY, width: bounds.width, height: 1))
        
        layer.shadowPath = shadowPath.cgPath
    }
    
    override func layoutSubviews() {
        let shadowPath = UIBezierPath(rect: CGRect(x: bounds.minX, y: bounds.maxY, width: bounds.width, height: 1))
        layer.shadowPath = shadowPath.cgPath
    }
    
}
