//
//  GradientBackgroundNavigationViewController.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 11.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

class GradientBackgroundNavigationController: UINavigationController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
//        self.navigationBar.tintColor = UIColor.darkText
//        self.navigationBar.barTintColor = UIColor.white
//        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
//        self.navigationBar.setBackgroundImage(imageLayerForGradientBackground(), for: .default)
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.mainColor
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

    }
    
    private func imageLayerForGradientBackground() -> UIImage {
        var updatedFrame = self.navigationBar.bounds
        updatedFrame.size.height += UIApplication.shared.statusBarFrame.height
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = image {
            return image
        } else {
            return UIImage()
        }
    }
}
