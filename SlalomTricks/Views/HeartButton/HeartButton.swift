//
//  HeartButton.swift
//  HeartButton
//
//  Created by Stanisław Paśkowski on 09.05.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

class HeartButton: UIControl {
    
    fileprivate let heartLayer = HeartLayer()
    var animationEnabled = false
    var isFavorite = false {
        didSet {
            updateColors()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        prepareButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareButton()
    }
    
    private func prepareButton() {
        self.layer.addSublayer(heartLayer)
        addTargets()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        heartLayer.frame = self.bounds
    }
    
    @objc private func favorite() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 16.0, options: [], animations: {
            self.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
            self.heartLayer.switchColors()
        }, completion: { (finished) in
        })
        
    }
    
    fileprivate func notFavorite() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 16.0, options: [], animations: {
            self.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
            self.heartLayer.switchColors()
        }, completion: { (finished) in
        })
    }
    
    fileprivate func backToStart(completionHandler: @escaping ()->()) {
        UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: [], animations: {
            self.transform = CGAffineTransform.identity
            
        }, completion: { (finished) in
            completionHandler()
        })
    }
    
    fileprivate func backToStart() {
        UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: [], animations: {
            self.transform = CGAffineTransform.identity
        })
    }
    
    public func addTargets() {
        self.addTarget(self, action: #selector(switchFavoriteState as (Void) -> Void), for: .touchUpInside)
    }
    
    public func updateColors() {
        if self.isFavorite {
            heartLayer.paintRed()
        } else {
            heartLayer.paintGray()
        }
    }
    
    public func switchFavoriteState() {
        if animationEnabled {
            if self.isFavorite {
                notFavorite()
                backToStart()
            } else {
                favorite()
                backToStart()
            }
        } else {
            heartLayer.switchColors()
        }
        self.isFavorite = !self.isFavorite
    }
    
    public func switchFavoriteState(completion: @escaping () -> ()) {
        if animationEnabled {
            if self.isFavorite {
                notFavorite()
                backToStart {
                    completion()
                }
            } else {
                favorite()
                backToStart {
                    completion()
                }
            }
        } else {
            heartLayer.switchColors()
        }
        self.isFavorite = !self.isFavorite
    }
    
}

//fileprivate extension Selector {
//        static let switchFavoriteState =
//            #selector(HeartButton.switchFavoriteState as (Void) -> Void)
//}
