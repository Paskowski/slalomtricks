//
//  HeartLayer.swift
//  HeartButton
//
//  Created by Stanisław Paśkowski on 09.05.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

class HeartLayer: CAShapeLayer {
    
    override var bounds: CGRect {
        didSet {
            let heartPath = UIBezierPath()
            heartPath.move(to: CGPoint(x: 15.5, y: 29.96))
            heartPath.addCurve(to: CGPoint(x: 0.28, y: 6.77), controlPoint1: CGPoint(x: 15.5, y: 29.96), controlPoint2: CGPoint(x: -2.44, y: 15.83))
            heartPath.addCurve(to: CGPoint(x: 9.1, y: 0), controlPoint1: CGPoint(x: 1.31, y: 2.86), controlPoint2: CGPoint(x: 4.86, y: 0))
            heartPath.addCurve(to: CGPoint(x: 15.5, y: 3.17), controlPoint1: CGPoint(x: 11.6, y: 0), controlPoint2: CGPoint(x: 13.86, y: 1.51))
            heartPath.addCurve(to: CGPoint(x: 21.9, y: 0), controlPoint1: CGPoint(x: 17.14, y: 1.51), controlPoint2: CGPoint(x: 19.4, y: 0))
            heartPath.addCurve(to: CGPoint(x: 30.72, y: 6.77), controlPoint1: CGPoint(x: 26.14, y: 0), controlPoint2: CGPoint(x: 29.69, y: 2.86))
            heartPath.addCurve(to: CGPoint(x: 15.5, y: 29.96), controlPoint1: CGPoint(x: 33.44, y: 15.83), controlPoint2: CGPoint(x: 15.5, y: 29.96))
            heartPath.close()
            heartPath.cgPath = CGPath.rescaleForFrame(path: heartPath.cgPath, frame: frame)
            path = heartPath.cgPath
        }
    }
    
    override init() {
        super.init()
        prepareLayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareLayer()
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
        prepareLayer()
    }
    
    fileprivate func prepareLayer() {
        paintGray()
    }
    
    public func paintGray() {
        fillColor = UIColor.clear.cgColor
        strokeColor = UIColor.gray.cgColor
    }
    
    public func paintRed() {
        fillColor = UIColor.clear.cgColor
        strokeColor = UIColor.heartRed.cgColor
    }
    
    public func switchColors() {
        if fillColor == UIColor.clear.cgColor {
            paintRed()
        } else {
            paintGray()
        }
    }
}

extension CGPath {
    class func rescaleForFrame(path: CGPath, frame: CGRect) -> CGPath {
        let boundingBox = path.boundingBoxOfPath
        let boundingBoxAspectRatio = boundingBox.width/boundingBox.height
        let viewAspectRatio = frame.width/frame.height
        
        var scaleFactor: CGFloat = 1
        if boundingBoxAspectRatio > viewAspectRatio {
            scaleFactor = frame.width/boundingBox.width
        } else {
            scaleFactor = frame.height/boundingBox.height
        }
        
        var scaleTransform = CGAffineTransform.identity
        scaleTransform = scaleTransform.scaledBy(x: scaleFactor, y: scaleFactor)
        scaleTransform = scaleTransform.translatedBy(x: -boundingBox.minX, y: -boundingBox.minY)
        let scaledSize = boundingBox.size.applying(CGAffineTransform(scaleX: scaleFactor, y: scaleFactor))
        let centerOffset = CGSize(width: (frame.width - scaledSize.width) / (scaleFactor * 2.0),
                                  height: (frame.height - scaledSize.height) / (scaleFactor * 2.0))
        scaleTransform = scaleTransform.translatedBy(x: centerOffset.width, y: centerOffset.height)
        
        if let scaleTransform = path.copy(using: &scaleTransform) {
            return scaleTransform
        } else {
            return path
        }
    }
}
