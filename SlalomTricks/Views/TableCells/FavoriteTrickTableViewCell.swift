//
//  FavoriteTrickTableViewCell.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 09.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class FavoriteTrickTableViewCell: MGSwipeTableCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var difficultyIndicatorView: DifficultyIndicatorView!
    @IBOutlet weak var progressIndicatorView: ProgressIndicatorView!
    var trick: Trick?
}
