//
//  TrickTableViewCell.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 11.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class TrickTableViewCell: MGSwipeTableCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heartButton: HeartButton!
    @IBOutlet weak var difficultyIndicatorView: DifficultyIndicatorView!
    var trick: Trick?

}
