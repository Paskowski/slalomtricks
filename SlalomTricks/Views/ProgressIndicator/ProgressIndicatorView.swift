//
//  ProgressIndicatorView.swift
//  ProgressIndicator2
//
//  Created by Stanisław Paśkowski on 29.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

protocol ProgressIndicatorViewDelegate: class {
    func didChangeCurrentProgress(value: CGFloat)
}

@IBDesignable
class ProgressIndicatorView: UIView {
    
    fileprivate let progressCircleLayer = ProgressCircleLayer()
    fileprivate let backgroundCircleLayer: BackgroundCircleLayer = BackgroundCircleLayer()
    fileprivate let progressLabel = UILabel()
    public weak var delegate: ProgressIndicatorViewDelegate?
    public var showPercentage = false {
        didSet {
            progressLabel.isHidden = !showPercentage
        }
    }
    
    public var maxValue: CGFloat = 5
    public var currentValue: CGFloat = 1 {
        didSet {
            if currentValue > maxValue {
                self.currentValue = maxValue
                setProgress(oldValue: oldValue, currentValue: currentValue)
            } else if currentValue < 0 {
                self.currentValue = 0
                setProgress(oldValue: oldValue, currentValue: currentValue)
            } else {
                setProgress(oldValue: oldValue, currentValue: currentValue)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareIndicatorView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareIndicatorView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundCircleLayer.bounds = CGRect(x: 0, y: 0, width: bounds.height, height: bounds.height)
        backgroundCircleLayer.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        progressCircleLayer.bounds = CGRect(x: 0, y: 0, width: bounds.height, height: bounds.height)
        progressCircleLayer.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        
    }
    
    private func prepareIndicatorView() {
        progressCircleLayer.progressDelegate = self
        self.layer.addSublayer(backgroundCircleLayer)
        self.layer.addSublayer(progressCircleLayer)
        
        addSubview(progressLabel)
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        progressLabel.font = progressLabel.font.withSize(40)
        progressLabel.text = "\(currentValue)"
        
        NSLayoutConstraint.activate([
            progressLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            progressLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
        
        if showPercentage {
            progressLabel.isHidden = false
        } else {
            progressLabel.isHidden = true
        }
    }
    
    public func animateIntro() {
//        progressCircleLayer.animateIntro()
//        backgroundCircleLayer.animateIntro()
    }
    
    public func disableIntroAnimation() {
        progressCircleLayer.introAnimationDisabled(value: true)
    }
    
    public func enableAnimations() {
        progressCircleLayer.introAnimationDisabled(value: false)
    }
    
    private func setProgress(oldValue: CGFloat, currentValue: CGFloat) {
        progressCircleLayer.currentValue = currentValue
    }
    
}

extension ProgressIndicatorView: ProgressCircleLayerDelegate {
    func didChangeCurrrent(value: CGFloat) {
        progressLabel.text = String(format: "%0.f%%", value)
    }
    
    func didSetInitial(value: CGFloat) {
        let formatedValue = value * 20
        progressLabel.text = String(format: "%0.f%%", formatedValue)
    }
    
    func didFinishChangingCurrent(value: CGFloat) {
        delegate?.didChangeCurrentProgress(value: value)
    }
}



