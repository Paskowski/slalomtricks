//
//  BackgroundCircleLayer.swift
//  ProgressIndicator2
//
//  Created by Stanisław Paśkowski on 29.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

class BackgroundCircleLayer: ProgressCircleLayer {
    
    override var bounds: CGRect {
        didSet {
            let center = CGPoint(x: bounds.width/2, y: bounds.height/2)
            let radius: CGFloat = max(bounds.width, bounds.height)
            
            let startAngle: CGFloat = 3/2 * CGFloat.pi
            let endAngle = startAngle + (2 * CGFloat.pi)
            
            path = UIBezierPath(arcCenter: center, radius: radius/2 - arcWidth/2, startAngle: startAngle, endAngle: endAngle, clockwise: true).cgPath
        }
    }
    
    override init() {
        super.init()
        prepareLayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareLayer()
    }
    
    func prepareLayer() {
        lineCap = kCALineCapRound
        lineWidth = arcWidth
        strokeColor = UIColor.gray.cgColor
        fillColor = UIColor.clear.cgColor
        strokeEnd = 1.0
    }
    
    public override func animateIntro() {
        strokeEnd = 0
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = strokeEnd
        animation.toValue = 1.0
        animation.duration = 0.9
        animation.fillMode = kCAFillModeForwards
        strokeEnd = 1
        add(animation, forKey: "animation")
    }
}
