//
//  ProgressCircleLayer.swift
//  ProgressIndicator2
//
//  Created by Stanisław Paśkowski on 29.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import QuartzCore

protocol ProgressCircleLayerDelegate: class {
    func didChangeCurrrent(value: CGFloat)
    func didSetInitial(value: CGFloat)
    func didFinishChangingCurrent(value: CGFloat)
}

class ProgressCircleLayer: CAShapeLayer {
    
    fileprivate var displayLink: CADisplayLink?
    public weak var progressDelegate: ProgressCircleLayerDelegate?
    public var maxValue: CGFloat = 5
    public var introAnimationDisabled = true
    public var currentValue: CGFloat = 1 {
        didSet {
            
            if introAnimationDisabled {
                strokeEnd = currentValue/maxValue
                progressDelegate?.didSetInitial(value: currentValue)
                introAnimationDisabled = false
            } else {
                animateChangeFor(newValue: currentValue)
            }
        }
    }
    public let arcWidth: CGFloat = 2
    
    override var bounds: CGRect {
        didSet {
            let center = CGPoint(x: bounds.width/2, y: bounds.height/2)
            let radius: CGFloat = max(bounds.width, bounds.height)
            
            let startAngle: CGFloat = 3/2 * CGFloat.pi
            let endAngle = startAngle + 2 * CGFloat.pi
            
            path = UIBezierPath(arcCenter: center, radius: radius/2 - arcWidth/2, startAngle: startAngle, endAngle: endAngle, clockwise: true).cgPath
        }
    }
    
    override init() {
        super.init()
        prepareLayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareLayer()
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
        prepareLayer()
    }
    
    private func prepareLayer() {
        lineCap = kCALineCapRound
        lineWidth = arcWidth
        strokeColor = UIColor.mainColor.cgColor
        fillColor = UIColor.clear.cgColor
        strokeEnd = currentValue / maxValue
    }
    
    public func animateIntro() {
            strokeEnd = 0
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.delegate = self
            animation.fromValue = strokeEnd
            animation.toValue = currentValue / maxValue
            animation.duration = 0.8
            animation.fillMode = kCAFillModeForwards
            strokeEnd = currentValue / maxValue
            add(animation, forKey: "animation")
    }
    
    private func animateChangeFor(newValue: CGFloat) {
        if let animationKeys = animationKeys() {
            if animationKeys.contains("animation") {
                changeAnimationToNew(endValue: newValue)
            }
        } else {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.delegate = self
            animation.fromValue = strokeEnd
            animation.toValue = newValue/maxValue
            animation.duration = 0.9
            animation.fillMode = kCAFillModeForwards
            createDisplayLink()
            strokeEnd = newValue/maxValue
            add(animation, forKey: "animation")
        }
    }
    
    private func changeAnimationToNew(endValue: CGFloat) {
        removeAnimation(forKey: "animation")
        let presentationLayer = presentation()
        let strokeEndValue = presentationLayer?.value(forKey: "strokeEnd")
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.delegate = self
        animation.fromValue = strokeEndValue
        animation.toValue = endValue/maxValue
        animation.duration = 0.9
        animation.fillMode = kCAFillModeForwards
        strokeEnd = endValue/maxValue
        add(animation, forKey: "animation")
    }
    
    private func createDisplayLink() {
        displayLink = CADisplayLink(target: self, selector: #selector(didChangeCurrentValue))
        guard let displayLink = displayLink else {return}
        displayLink.preferredFramesPerSecond = 45
        displayLink.add(to: .main, forMode: .commonModes)
    }
    
    @objc private func didChangeCurrentValue() {
        let strokeEndValue = presentation()?.value(forKey: "strokeEnd") as! CGFloat
        progressDelegate?.didChangeCurrrent(value: strokeEndValue * 100)
    }
    
    public func introAnimationDisabled(value: Bool) {
        self.introAnimationDisabled = value
    }
}

extension ProgressCircleLayer: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        guard let displayLink = displayLink else {return}
        if flag {
            progressDelegate?.didFinishChangingCurrent(value: currentValue)
            displayLink.invalidate()
        }
    }
}

