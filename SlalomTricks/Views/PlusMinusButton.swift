//
//  PlusMinusButton.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 04.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

@IBDesignable class PlusMinusButton: UIButton {
    
    @IBInspectable var fillColor: UIColor = UIColor.gray
    @IBInspectable var isAddButton: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(ovalIn: rect)
        UIColor.white.setFill()
        path.fill()
        
        // set up the width and height variables
        // for horizontal stroke
        let plusHeight: CGFloat = 2.0
        let plusWidth: CGFloat = min(bounds.width, bounds.height) * 0.6
        
        // create the path
        let plusPath = UIBezierPath()
        
        // set the path's line width to the height of the stroke
        plusPath.lineWidth = plusHeight
        
        // move the initial point of the path
        // to the start of the horizontal stroke
        plusPath.move(to: CGPoint(
            x: bounds.width/2 - plusWidth/2,
            y: bounds.height/2))
        
        //add a point to the path at the end of the stroke
        plusPath.addLine(to: CGPoint(
            x:bounds.width/2 + plusWidth/2,
            y:bounds.height/2))
        
        // set the stroke color
        UIColor.mainColor.setStroke()
        // draw the stroke
        plusPath.lineCapStyle = .round
        plusPath.stroke()
        
        if isAddButton {
            let minusPath = UIBezierPath()
            
            minusPath.lineWidth = plusHeight
            
            minusPath.move(to: CGPoint(x: bounds.width/2, y: bounds.height/2 - plusWidth/2))
            minusPath.addLine(to: CGPoint(x: bounds.width/2, y: bounds.height/2 + plusWidth/2))
            minusPath.lineCapStyle = .round
            minusPath.stroke()
        }
    }
}
