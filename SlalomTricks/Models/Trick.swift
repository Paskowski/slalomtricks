//
//  Trick.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 11.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import Foundation
import Firebase

struct Trick {
    let name: String
    let clipUrl: String
    let difficulty: Int
    var isFavorite: Bool?
    var progressLevel: Int?
    
    init(name: String, clipUrl: String, difficulty: Int, progressLevel: Int = 0, isFavorite: Bool = false) {
        self.name = name
        self.clipUrl = clipUrl
        self.difficulty = difficulty
        self.progressLevel = progressLevel
        self.isFavorite = isFavorite
    }
//    
//    init(snapshot: FIRDataSnapshot) {
//        let snapshotValue = snapshot.value as! [String: AnyObject]
//        name = snapshotValue["name"] as! String
//        clipUrl = snapshotValue["clipUrl"] as! String
//        difficulty = snapshotValue["difficulty"] as! Int
//    }
//    
//    init(snapshot: FIRDataSnapshot, isFavorite: Bool) {
//        let snapshotValue = snapshot.value as! [String: AnyObject]
//        name = snapshotValue["name"] as! String
//        clipUrl = snapshotValue["clipUrl"] as! String
//        difficulty = snapshotValue["difficulty"] as! Int
//        self.isFavorite = isFavorite
//        currentLevel = 0
//    }
    
    init?(snapshot: FIRDataSnapshot) {
        guard
            let snapshotValue: [String: AnyObject] = snapshot.value as? [String: AnyObject],
            let name: String = snapshotValue["name"] as? String,
            let clipUrl: String = snapshotValue["clipUrl"] as? String,
            let difficulty: Int = snapshotValue["difficulty"] as? Int
        else { return nil }
        
        let progressLevel: Int = snapshotValue["progressLevel"] as? Int ?? 0
        let isFavorite: Bool = snapshotValue["isFavorite"] as? Bool ?? false
        
        self.init(name: name, clipUrl: clipUrl, difficulty: difficulty, progressLevel: progressLevel, isFavorite: isFavorite)
    }
    
    init?(snapshot: FIRDataSnapshot, isFavorite: Bool) {
        self.init(snapshot: snapshot)
        self.isFavorite = isFavorite
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name,
            "clipUrl": clipUrl,
            "difficulty": difficulty,
            "progressLevel": progressLevel ?? 0,
            "isFavorite": isFavorite ?? false
        ]
    }
    
    func toDictionary() -> [AnyHashable: Any] {
        return [
            "name": name,
            "clipUrl": clipUrl,
            "difficulty": difficulty,
            "progressLevel": progressLevel ?? 0,
            "isFavorite": isFavorite ?? false
        ]
    }
    
    mutating func setFavorite(to value: Bool) {
        self.isFavorite = value
    }
}

extension Trick: Equatable {}
    func ==(lhs: Trick, rhs: Trick) -> Bool {
    return lhs.name == rhs.name
}

extension Trick: Hashable {
     var hashValue: Int { get { return name.hashValue } }
}
