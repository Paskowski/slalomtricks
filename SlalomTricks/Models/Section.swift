//
//  Section.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 05.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import Foundation

struct Section {
    
    var heading : String
    var locations : [Location]
    
    init(title: String, locations : [Location]) {
        
        self.heading = title
        self.locations = locations
    }
}
