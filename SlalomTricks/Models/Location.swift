//
//  Location.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 05.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import Foundation
import MapKit
import Firebase

struct Location {
    let name: String
    let coordinates: CLLocation
    
    init(name: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.name = name
        self.coordinates = CLLocation(latitude: latitude, longitude: longitude)
    }
    
    init?(snapshot: FIRDataSnapshot) {
        guard
            let snapshotValue: [String: AnyObject] = snapshot.value as? [String: AnyObject],
            let name: String = snapshotValue["name"] as? String,
            let longitude: CLLocationDegrees = snapshotValue["longitude"] as? CLLocationDegrees,
            let latitude: CLLocationDegrees = snapshotValue["latitude"] as? CLLocationDegrees
        else {return nil}
        
        self.init(name: name, latitude: latitude, longitude: longitude)
    }
}
