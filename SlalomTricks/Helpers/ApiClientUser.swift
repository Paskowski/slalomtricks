//
//  ApiClientUser.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 11.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import Foundation

protocol ApiClientUser {
    func set(apiClient: APIClient)
}
