//
//  Helpers.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 05.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        
        let darkerBlue = UIColor(red:0.17, green:0.56, blue:0.79, alpha:1.0)
        let lighterBlue = UIColor(red:0.24, green:0.70, blue:0.97, alpha:1.0)
        
        layer.colors = [darkerBlue.cgColor, lighterBlue.cgColor]
        return layer
    }
}

extension Array where Element: Equatable {
    
    public func uniq() -> [Element] {
        var arrayCopy = self
        arrayCopy.uniqInPlace()
        return arrayCopy
    }
    
    mutating public func uniqInPlace() {
        var seen = [Element]()
        var index = 0
        for element in self {
            if seen.contains(element) {
                guard let trick = element as? Trick else {return}
                if trick.isFavorite != nil {
                    let indexToRemove = seen.index(of: element)
                    seen[indexToRemove!] = element
                }
                remove(at: index)
            } else {
                seen.append(element)
                index += 1
            }
        }
    }
}

extension UIColor {
    
    @nonobjc static let mainColor = UIColor(red:0.20, green:0.60, blue:0.86, alpha:1.0)
    @nonobjc static let heartRed = UIColor(red:0.96, green:0.17, blue:0.45, alpha:1.0)
    @nonobjc static let gray = UIColor(red:0.67, green:0.71, blue:0.74, alpha:1.0)
}
