//
//  APIClient.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 20.02.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import Foundation
import KeychainAccess
import Firebase

class APIClient: NSObject {
    
    // MARK: Properties
    
    let keychain = Keychain(service: "com.slalom-tricks")
    let firebaseAuthentication = FIRAuth.auth()
    let firebaseDB: FIRDatabaseReference = FIRDatabase.database().reference()
    var userId: String?
    var firebaseHandle: FIRDatabaseHandle?
    var firebaseHandles: [FIRDatabaseHandle] = []
    
    override init() {
        super.init()
        if let uid = keychain["uid"] {
            userId = uid
        }
    }

    // MARK: - Login related methods
    
    func loginUserWith(email: String, password: String, success: @escaping () -> (), failure: @escaping (Error?) -> ()) {
        firebaseAuthentication?.signIn(withEmail: email, password: password, completion: { (user, error) in
            guard error == nil else {
                failure(error!)
                return
            }
            
            if let user = user {
                self.userId = user.uid  // Unique ID, which you can use to identify the user on the client side
                do {
                    try self.keychain.set(self.userId!, key: "uid")
                } catch let error {
                    print("There was an error with adding token to Keychain \(error)")
                }
            }

            success()
        })
    }
    
    func registerUser(email: String, password: String, success: @escaping () -> (), failure: @escaping (Error?) -> ()) {
        firebaseAuthentication?.createUser(withEmail: email, password: password, completion: { (user, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            if let userId = self.userId {
                self.firebaseDB.child("Users/\(userId)").setValue(self.userId);
            }
            
            print("We've got user registed: \(String(describing: user))")
            self.firebaseAuthentication?.signIn(withEmail: email, password: password, completion: { (user, error) in
                guard error == nil else {
                    failure(error)
                    return
                }
                success()
            })
            
        })
    }
    
    // MARK: - Tricks related methods
    
    func get(allTricks: @escaping ([Trick]) -> ()) {
        firebaseDB.child("Tricks").observeSingleEvent(of: .value, with: { (snapshot) in
            var tricks: [Trick] = []
            for trick in snapshot.children {
                guard
                    let trickSnapshot: FIRDataSnapshot = trick as? FIRDataSnapshot,
                    let trick: Trick = Trick(snapshot: trickSnapshot, isFavorite: false)
                else {return}
                // Return czy continue? Przy continue wyświetla się lista tricków, ale czy nie pozwala to na potencjalny błąd w innym miejscu?
                tricks.append(trick)
            }
            allTricks(tricks)
        })
    }
    
    func getMyTricks(completion: @escaping ([Trick]) -> ()) {
        guard let userId = userId else {return}
        firebaseDB.child("Users/\(userId)/MyTricks").observeSingleEvent(of: .value, with: { (snapshot) in
            var tricks: [Trick] = []
            tricks.removeAll(keepingCapacity: true)
            for trick in snapshot.children {
                guard
                    let trickSnapshot: FIRDataSnapshot = trick as? FIRDataSnapshot,
                    let trick: Trick = Trick(snapshot: trickSnapshot)
                    else {return}
                tricks.append(trick)
            }
            completion(tricks)
        })
    }
    
    func getAllTricksWithMyTricks(completion: @escaping ([Trick]) -> ()) {
        get(allTricks: { (allTricks) in
            self.getMyTricks(completion: { (myTricks) in
                var allTricks: [Trick] = allTricks
                for (index, trick) in allTricks.enumerated() {
                    for myTrick in myTricks {
                        if trick.name == myTrick.name {
                            allTricks[index] = myTrick
                        }
                        // marking unfavorited tricks back from favorited
                        if trick.isFavorite != nil && !myTricks.contains(trick) {
                            allTricks[index].isFavorite = false
                        }
                    }
                }
                completion(allTricks)
            })
        })
    }
    
    func get(favoriteTricks: @escaping ([Trick]) -> ()) {
        var tricks: [Trick] = []
        guard let userId = userId else {return}
        firebaseHandle = firebaseDB.child("Users/\(userId)/MyTricks").queryOrdered(byChild: "isFavorite").queryEqual(toValue: true).observe(.value, with: { (snapshot) in
            tricks.removeAll(keepingCapacity: true)
            for trick in snapshot.children {
                guard
                    let trickSnapshot: FIRDataSnapshot = trick as? FIRDataSnapshot,
                    let trick: Trick = Trick(snapshot: trickSnapshot, isFavorite: true)
                else {return}
                tricks.append(trick)
            }
            favoriteTricks(tricks)
        })
    }
    
    func observeTrickAddition(completion: @escaping (Trick) -> ()) {
        guard let userId = userId else {return}
        firebaseDB.child("Users/\(userId)/MyTricks").observe(.childAdded, with: { (snapshot) in
            guard let trick: Trick = Trick(snapshot: snapshot) else {return}
            completion(trick)
        })
    }
    
    func observeTrickChanges(modifiedTrick: @escaping (Trick) -> ()) {
        guard let userId = userId else {return}
        firebaseDB.child("Users/\(userId)/MyTricks").observe(.childChanged, with: { (snapshot) in
            guard let trick: Trick = Trick(snapshot: snapshot) else {return}
            modifiedTrick(trick)
        })
        
//        firebaseDB.child("Users/\(userId)/MyTricks").observe(.childRemoved, with: { (snapshot) in
//            print("There was a child removal")
//            guard let trick: Trick = Trick(snapshot: snapshot) else {return}
//            deletedTrick(trick)
//        })

    }
    
    func removeFromMyTricksIfNecessary(trick: Trick) {
        if trick.progressLevel == 0 && trick.isFavorite == false {
            guard let userId = userId else {return}
            firebaseDB.child("Users/\(userId)/MyTricks/\(trick.name))").updateChildValues([:])
        }
    }
    
    func trickIsInMyTricks(trick: Trick, result: @escaping (Bool) -> ()) {
        guard let userId = userId else {return}
        firebaseDB.child("Users/\(userId)/MyTricks/\(trick.name)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                result(true)
            } else {
                result(false)
            }
        })
    }
    
    func markAsFavorite(trick: Trick) {
        guard let userId = userId else {return}
        firebaseDB.child("Users/\(userId)/MyTricks/\(trick.name)").updateChildValues(trick.toDictionary())
    }
    
    func unmarkFavorite(trick: Trick) {
        guard let userId = userId else {return}
        firebaseDB.child("Users/\(userId)/MyTricks/\(trick.name)").updateChildValues(["isFavorite": false])
        removeFromMyTricksIfNecessary(trick: trick)
    }
    
    func updateProgressLevel(for trick: Trick) {
        guard let userId = userId else {return}
        trickIsInMyTricks(trick: trick) { (result) in
            if result == true {
                self.firebaseDB.child("Users/\(userId)/MyTricks/\(trick.name)").updateChildValues(["progressLevel": trick.progressLevel!])
            } else {
                self.firebaseDB.child("Users/\(userId)/MyTricks/\(trick.name)").updateChildValues(trick.toDictionary())
            }
        }
    }
    
    func getLocations(completionHandler: @escaping (([Section]) ->())) {
        firebaseDB.child("Locations").observe(.value, with: { (snapshot) in
            var sections: [Section] = []
            for city in snapshot.children {
                guard let city: FIRDataSnapshot = city as? FIRDataSnapshot else {return}
                
                var locations: [Location] = []
                for location in city.children {
                    guard let location: FIRDataSnapshot = location as? FIRDataSnapshot else {return}
                    guard let newLocation: Location = Location(snapshot: location) else {return}
                    locations.append(newLocation)
                }
                let section: Section = Section(title: city.key, locations: locations)
                sections.append(section)
            }
            completionHandler(sections)
        })
    }
}
