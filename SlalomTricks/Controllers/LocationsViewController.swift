//
//  PlacesViewController.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 05.04.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import MapKit

class LocationsViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var tableViewHeightInitialValue: CGFloat = 0
    var apiClient: APIClient?
    var sections: [Section] = []
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        mapView.delegate = self
        guard let apiClient: APIClient = apiClient else {return}
        
        apiClient.getLocations { (sections) in
            DispatchQueue.main.async {
                self.sections = sections
                self.tableView.reloadData()
                
                for section in sections {
                    for location in section.locations {
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = location.coordinates.coordinate
                        self.mapView.addAnnotation(annotation)
                    }
                }
            }
        }
        
        tableViewHeightInitialValue = tableViewHeightConstraint.constant
    }
    
    override func viewDidLayoutSubviews() {
        setupShadows()
    }
    
    let regionRadius: CLLocationDistance = 1000
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func createLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) -> MKPointAnnotation {
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        return annotation
    }
    
    func setupShadows() {
        // tune this shadow
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 1.2)
        shadowView.layer.shadowRadius = 1.2
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shouldRasterize = true
        
        let shadowPath = UIBezierPath(rect: CGRect(x: shadowView.bounds.minX, y: shadowView.bounds.minY, width: shadowView.bounds.width, height: shadowView.bounds.height))
        
        shadowView.layer.shadowPath = shadowPath.cgPath
        self.view.sendSubview(toBack: tableView)
    }
    
    @IBAction func didTapHideMap(_ sender: Any) {
//        tableViewHeightConstraint.constant = 0
        if tableViewHeightConstraint.constant == tableViewHeightInitialValue {
            tableViewHeightConstraint.constant = 0
        } else {
            tableViewHeightConstraint.constant = tableViewHeightInitialValue
        }
        UIView.animate(withDuration: 0.7, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
}

extension LocationsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].locations.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].heading
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath)
        cell.textLabel?.text = sections[indexPath.section].locations[indexPath.row].name
        return cell
    }
}

extension LocationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        centerMapOnLocation(location: sections[indexPath.section].locations[indexPath.row].coordinates)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else {return}
        header.textLabel?.textColor = UIColor.white
        header.backgroundView?.backgroundColor = UIColor.mainColor
    }
}

extension LocationsViewController: ApiClientUser {
    func set(apiClient: APIClient) {
        self.apiClient = apiClient
    }
}

extension LocationsViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseId = "MapMarker"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if let pinView = pinView {
            pinView.annotation = annotation
        } else {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView?.image = StyleKit.imageOfCone()
        }
        return pinView
    }
}
