//
//  FavoriteTricksViewController.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 28.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class FavoriteTricksViewController: TricksViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        guard let apiClient = apiClient else { return }
        
        apiClient.get(favoriteTricks: { (favoriteTricks) in
            DispatchQueue.main.async {
                self.tricksList = favoriteTricks
                self.tricksTableView.reloadData()
            }
        })
    }
    
    override func viewDidLoad() {
        tricksTableView.delegate = self
        tricksTableView.dataSource = self
        tricksTableView.tableFooterView = UIView(frame: .zero)
        setupSearchBar()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: .didTapSearchButton)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        apiClient?.firebaseDB.removeObserver(withHandle: (apiClient?.firebaseHandle!)!)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTrickCell") as? FavoriteTrickTableViewCell else {
            return UITableViewCell()
        }
        let trick: Trick
        if searchBar.isFirstResponder && searchBar.text != "" {
            trick = filteredTricks[indexPath.row]
        } else {
            trick = tricksList[indexPath.row]
        }
        
        cell.delegate = self
        cell.nameLabel.text = trick.name
        cell.trick = trick
        cell.difficultyIndicatorView.difficultyLevel = trick.difficulty
        if let progressValue = trick.progressLevel {
            cell.progressIndicatorView.currentValue = CGFloat (progressValue)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let trick: Trick
        if searchBar.isFirstResponder && searchBar.text != "" {
            trick = filteredTricks[indexPath.row]
        } else {
            trick = tricksList[indexPath.row]
        }
        performSegue(withIdentifier: "showFavoriteTrickDetails", sender: trick)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? TrickDetailsViewController, segue.identifier == "showFavoriteTrickDetails", let trick: Trick = sender as? Trick, let apiClient = apiClient {
            vc.trick = trick
            vc.set(apiClient: apiClient)
        }
    }
    
    override func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        _ = super.swipeTableCell(cell, swipeButtonsFor: direction, swipeSettings: swipeSettings, expansionSettings: expansionSettings)
        
        guard let cell: FavoriteTrickTableViewCell = cell as? FavoriteTrickTableViewCell, let trickIndexPath: IndexPath = tricksTableView.indexPath(for: cell) else {return [MGSwipeTableCell()]}
        
        var trick: Trick = trickFor(indexPath: trickIndexPath)
        
        if direction == MGSwipeDirection.leftToRight {
            expansionSettings.expansionColor = UIColor.red
            return [
                MGSwipeButton(title: "", icon: UIImage(named: "HeartWhite"), backgroundColor: UIColor.gray, callback: { (swipeCell) -> Bool in
                    trick.setFavorite(to: false)
                    self.apiClient?.markAsFavorite(trick: trick)
                    if self.isFilteringInProgress() {
                        self.filteredTricks[trickIndexPath.row].setFavorite(to: true)
                        self.filteredTricks.remove(at: trickIndexPath.row)
                    } else {
                        self.tricksList[trickIndexPath.row].setFavorite(to: false)
                        self.tricksList.remove(at: trickIndexPath.row)
                    }
                    self.tricksTableView.deleteRows(at: [trickIndexPath], with: .left)
                    return true
                })
            ]
        } else {
            return nil
        }
    }
}

