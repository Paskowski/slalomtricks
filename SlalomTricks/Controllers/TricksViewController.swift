//
//  TricksViewController.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 09.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class TricksViewController: UIViewController {
    
    // MARK: - Properties
    
    var apiClient: APIClient?
    var tricksList: [Trick] = []
    var filteredTricks: [Trick] = []
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var tricksTableView: UITableView!
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    var initialHeaderHeight: CGFloat = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let apiClient = apiClient else { print("No client yet")
            return}
        
        apiClient.observeTrickAddition { (addedTrick) in
            self.handleModified(trick: addedTrick)
        }
        
        apiClient.observeTrickChanges { (modifiedTrick) in
            self.handleModified(trick: modifiedTrick)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tricksTableView.delegate = self
        tricksTableView.dataSource = self
        
        guard let apiClient = apiClient else { return }
        apiClient.getAllTricksWithMyTricks { (tricks) in
            DispatchQueue.main.async {
                self.tricksList = tricks
                self.tricksTableView.reloadData()
            }
        }
        
        setupSearchBar()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: .didTapSearchButton)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let apiClient = apiClient else {return}
        guard let firebaseHandle = apiClient.firebaseHandle else {return}
        apiClient.firebaseDB.removeObserver(withHandle: firebaseHandle)
        for handle in apiClient.firebaseHandles {
            apiClient.firebaseDB.removeObserver(withHandle: handle)
        }
    }
    
    func showSearchBar() {
        self.view.layoutIfNeeded()
        navigationController?.setNavigationBarHidden(true, animated: true)

        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant += self.initialHeaderHeight + UIApplication.shared.statusBarFrame.height
            self.searchBar.alpha = 1
            self.view.layoutIfNeeded()
        }) { (Bool) in
            self.searchBar.becomeFirstResponder()
        }
    }
    
    func hideSearchBar() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant -= (self.initialHeaderHeight + UIApplication.shared.statusBarFrame.height)
            self.searchBar.alpha = 0
            self.view.layoutIfNeeded()
        }) { (Bool) in
            self.searchBar.resignFirstResponder()
        }
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setupSearchBar() {
        initialHeaderHeight = headerHeightConstraint.constant
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.alpha = 0
        searchBar.placeholder = "Nazwa triku"
        searchBar.backgroundImage = UIImage()
        searchBar.barTintColor = UIColor.white
        header.backgroundColor = UIColor.mainColor
        let attributes = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont.systemFont(ofSize: 17)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        headerHeightConstraint.constant -= initialHeaderHeight
    }
    
    private func handleModified(trick: Trick) {
        let trickToReloadIndexPath: IndexPath
        if self.tricksList.contains(trick) {
            if self.isFilteringInProgress() {
                guard let arrayIndexInFiltered = self.filteredTricks.index(of: trick), let arrayIndexInAllTricks = self.tricksList.index(of: trick) else {return}
                self.filteredTricks[arrayIndexInFiltered] = trick
                self.tricksList[arrayIndexInAllTricks] = trick
                trickToReloadIndexPath = IndexPath(row: arrayIndexInFiltered, section: 0)
            } else {
                guard let arrayIndexInAllTricks = self.tricksList.index(of: trick) else {return}
                self.tricksList[arrayIndexInAllTricks] = trick
                trickToReloadIndexPath = IndexPath(row: arrayIndexInAllTricks, section: 0)
            }
            
            DispatchQueue.main.async {
                self.tricksTableView.beginUpdates()
                self.tricksTableView.reloadRows(at: [trickToReloadIndexPath], with: .none)
                self.tricksTableView.endUpdates()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? TrickDetailsViewController, segue.identifier == "showTrickDetails", let trick: Trick = sender as? Trick {
            vc.trick = trick
            guard let apiClient = apiClient else {return}
            vc.set(apiClient: apiClient)
        }
    }
}

extension TricksViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let trick: Trick
        if searchBar.isFirstResponder && searchBar.text != "" {
            trick = filteredTricks[indexPath.row]
        } else {
            trick = tricksList[indexPath.row]
        }
        performSegue(withIdentifier: "showTrickDetails", sender: trick)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}

extension TricksViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.isFirstResponder && searchBar.text != "" {
            return filteredTricks.count
        } else {
            return tricksList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TrickCell", for: indexPath) as? TrickTableViewCell else {
            return UITableViewCell()
        }
        
        let trick: Trick
        if searchBar.isFirstResponder && searchBar.text != "" {
            trick = filteredTricks[indexPath.row]
        } else {
            trick = tricksList[indexPath.row]
        }
        cell.delegate = self
        cell.nameLabel.text = trick.name
        cell.selectionStyle = .none
        if let trickIsFavoriteState = trick.isFavorite {
            cell.heartButton.isFavorite = trickIsFavoriteState
        }
        cell.heartButton.animationEnabled = true
        cell.difficultyIndicatorView.difficultyLevel = trick.difficulty
        return cell
    }
    
    func trickFor(indexPath: IndexPath) -> Trick {
        let trick: Trick
        if searchBar.isFirstResponder && searchBar.text != "" {
            trick = filteredTricks[indexPath.row]
        } else {
            trick = tricksList[indexPath.row]
        }
        return trick
    }
    
    func isFilteringInProgress() -> Bool {
        if searchBar.isFirstResponder && searchBar.text != "" {
            return true
        } else {
            return false
        }
    }
    
    
}

extension TricksViewController: MGSwipeTableCellDelegate {

    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        swipeSettings.keepButtonsSwiped = false
        swipeSettings.transition = MGSwipeTransition.clipCenter
        expansionSettings.buttonIndex = 0
        expansionSettings.expansionLayout = .center
        expansionSettings.triggerAnimation.easingFunction = .cubicOut
        expansionSettings.threshold = 1.0
        expansionSettings.fillOnTrigger = false
        
        guard let cell: TrickTableViewCell = cell as? TrickTableViewCell, let trickIndexPath: IndexPath = tricksTableView.indexPath(for: cell) else {return [MGSwipeTableCell()]}

        var trick: Trick = trickFor(indexPath: trickIndexPath)
        guard let trickIsFavorite = trick.isFavorite else {return [MGSwipeTableCell()]}
        if direction == MGSwipeDirection.rightToLeft {
            expansionSettings.expansionColor = UIColor.mainColor
            return [
                MGSwipeButton(title: "", icon: UIImage(named: "HeartWhite"), backgroundColor: UIColor.gray, callback: { (swipeCell) -> Bool in
                    if !trickIsFavorite {
                        cell.heartButton.switchFavoriteState {
                            trick.setFavorite(to: true)
                            self.apiClient?.markAsFavorite(trick: trick)
                            if self.isFilteringInProgress() {
                                self.filteredTricks[trickIndexPath.row].setFavorite(to: false)
                            } else {
                                self.tricksList[trickIndexPath.row].setFavorite(to: true)
                            }
                        }
                    }
                    return true
                })
            ]
        } else if direction == MGSwipeDirection.leftToRight {
            expansionSettings.expansionColor = UIColor.red
            return [
                MGSwipeButton(title: "", icon: UIImage(named: "HeartWhite"), backgroundColor: UIColor.gray, callback: { (swipeCell) -> Bool in
                    if trickIsFavorite {
                        cell.heartButton.switchFavoriteState {
                            trick.setFavorite(to: false)
                            self.apiClient?.markAsFavorite(trick: trick)
                            if self.isFilteringInProgress() {
                                self.filteredTricks[trickIndexPath.row].setFavorite(to: true)
                            } else {
                                self.tricksList[trickIndexPath.row].setFavorite(to: false)
                            }
                        }
                    }
                    return true
                })
            ]
        } else {
            return nil
        }
    }
}

extension TricksViewController: ApiClientUser {
    func set(apiClient: APIClient) {
        self.apiClient = apiClient
    }
}

extension TricksViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        hideSearchBar()
        tricksTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredTricks = tricksList.filter({ (trick) -> Bool in
            return trick.name.lowercased().contains(searchText.lowercased())
        })
        tricksTableView.reloadData()
    }
    
}

extension Selector {
    static let didTapSearchButton = #selector(TricksViewController.showSearchBar)
}

