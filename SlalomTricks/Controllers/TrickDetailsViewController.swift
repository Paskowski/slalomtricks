//
//  TrickDetailsViewController.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 13.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import WebKit
import youtube_ios_player_helper
import SnapKit

class TrickDetailsViewController: UIViewController {
    var apiClient: APIClient?
    var trick: Trick?
    var navBarWasHidden: Bool = false
    @IBOutlet weak var youtubePlayer: YTPlayerView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var heartButton: HeartButton!
    @IBOutlet weak var difficultyIndicatorView: DifficultyIndicatorView!
    @IBOutlet weak var progressIndicatorView: ProgressIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        progressIndicatorView.showPercentage = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (navigationController?.navigationBar.isHidden)! {
            navigationController?.setNavigationBarHidden(false, animated: false)
            navBarWasHidden = true
        }
        progressIndicatorView.delegate = self
        
        guard let trick = trick else { return }
        titleLabel.text = trick.name
        youtubePlayer.load(withVideoId: trick.clipUrl, playerVars: ["modestbranding": 1, "showinfo": 0, "rel": 0])
        youtubePlayer.playVideo()
        
        difficultyIndicatorView.difficultyLevel = trick.difficulty
        guard let progressLevel = trick.progressLevel else {return}
        progressIndicatorView.currentValue = CGFloat (progressLevel)
        
        if let trickIsFavoriteState = trick.isFavorite {
            heartButton.isFavorite = trickIsFavoriteState
        }
        heartButton.animationEnabled = true
     
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (navBarWasHidden == true) {
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    @IBAction func didTapHeartButton(_ sender: Any) {
        guard
            let apiClient = apiClient,
            var trick = trick,
            let trickIsFavorite = trick.isFavorite
        else {return}
        if trickIsFavorite {
            trick.setFavorite(to: false)
        } else {
            trick.setFavorite(to: true)
        }
        apiClient.markAsFavorite(trick: trick)
    }
    
    @IBAction func didTapPlusButton(_ sender: Any) {
        progressIndicatorView.currentValue += 1
    }
    
    @IBAction func didTapMinusButton(_ sender: Any) {
        progressIndicatorView.currentValue -= 1
    }
}

extension TrickDetailsViewController: ProgressIndicatorViewDelegate {
    func didChangeCurrentProgress(value: CGFloat) {
        guard let
            apiClient = apiClient,
            var trick = trick
            else {return}
        trick.progressLevel = Int(value)
        apiClient.updateProgressLevel(for: trick)
    }
}

extension TrickDetailsViewController: ApiClientUser {
    func set(apiClient: APIClient) {
        self.apiClient = apiClient
    }
}

