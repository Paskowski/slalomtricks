//
//  ViewController.swift
//  SlalomTricks
//
//  Created by Stanisław Paśkowski on 20.02.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var skipLoginButton: UIButton!
    @IBOutlet weak var goToRegisterVCButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var borderBottom3: UIView!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var lostPassword: UIButton!
    
    let apiClient = APIClient()
    var gradientLayer: CAGradientLayer!
    
    // MARK: - Methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.view.layer.insertSublayer(CAGradientLayer.gradientLayerForBounds(bounds: self.view.bounds), at: 0)
//        self.view.backgroundColor = UIColor.mainColor
        loginButton.layer.cornerRadius = 2
        skipLoginButton.layer.cornerRadius = 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.text = "stanislaw.paskowski@gmail.com"
        passwordTextField.text = "password123"
        
        if title == "Login" {
            
        } else {
            titleLabel.text = "Rejestracja"
            loginButton.isHidden = true
            lostPassword.isHidden = true
            goToRegisterVCButton.isHidden = true
            skipLoginButton.isHidden = true
            
            repeatPasswordTextField.isHidden = false
            borderBottom3.isHidden = false
            registerButton.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc: UITabBarController = segue.destination as? UITabBarController, let apiClient = sender as? APIClient, segue.identifier == "showTricksTableView" {
            for child in vc.viewControllers! {
                guard let navigationController = child as? GradientBackgroundNavigationController else  { return }
                guard let topViewController = navigationController.topViewController as? ApiClientUser else { return }
                topViewController.set(apiClient: apiClient)
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            apiClient.loginUserWith(email: email, password: password, success: {
                self.performSegue(withIdentifier: "showTricksTableView", sender: self.apiClient)
            }, failure: { (error) in
                print("There was an error with logging: \(String(describing: error))")
            })
        }
    }
    
    @IBAction func didTapGoToRegisterVCButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController
        
        guard let loginViewController = vc else {return}
        
        loginViewController.title = "Register"
        present(loginViewController, animated: true)
    }
    
    @IBAction func didTapRegisterButton(_ sender: Any) {
        
        // TODO: Add all input-error related stuff
        
        if let email = emailTextField.text, let password = passwordTextField.text, let repeatedPassword = repeatPasswordTextField.text, password == repeatedPassword  {
            
            apiClient.registerUser(email: email, password: password, success: { _ in
                self.apiClient.loginUserWith(email: email, password: password, success: {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "showTricksTableView", sender: self.apiClient)
                    }
                }, failure: { (error) in
                    guard let error = error else {return}
                    print(error)
                })
                
            }, failure: { (error) in
                guard let error = error else {return}
                print(error)
            })
        }
    }
}

